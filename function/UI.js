﻿function UI() {

    this.Disable = function(buttonType) {
        var $button = $("#editor .toolbar .button." + buttonType);

        if (!$button.hasClass("disabled")) {
            $button.addClass("disabled");
        }
    }

    this.Enable = function(buttonType) {
        var $button = $("#editor .toolbar .button." + buttonType);
        $button.removeClass("disabled");
    }

    this.AllowSubmit = function() {
        var $button = $(".submitButton");
        $button.removeClass("disabled");
    }

    this.ButtonFlash = function() {
        var $button = $(".submitButton");
        $button.addClass("clicked");

        setTimeout(function() {
            var $button = $(".submitButton");
            $button.removeClass("clicked");
        }, 200);
    }


    this.timeoutObject = undefined;
}

UiManager = new UI();


$(document).ready(function() {
    paintManager.tool_change("pencil");

    $("#slider").slider({
        value: 5,
        min: 1,
        max: 14,
        step: 1,
        slide: function(event, ui) {
            if (event.originalEvent) {
                //manual change
                //alert(ui.value);
                $(this).css("height", ui.value);
                paintManager.setLineWidth(ui.value, true);
            } else {
                //programmatic change
            }
        }
    });
    var currentValue = $("#slider").slider('value');
    $("#slider").css("height", currentValue);
    paintManager.setLineWidth(currentValue, false);

    $("#editor .toolbar .button").click(function() {
        var isDisabled = $(this).hasClass("disabled");

        if (!isDisabled) {
            $("#editor .toolbar .button .accept").removeClass("clicked");
            $(this).addClass("clicked");

            paintManager.changeColor($(".color").val());
            paintManager.changeColor($(".accept"));
            var toolType = "pencil";
            if ($(this).hasClass("line")) {
                toolType = "line";
            } else if ($(this).hasClass("rectangle")) {
                toolType = "rect";
            } else if ($(this).hasClass("ellipse")) {
                toolType = "circle";
            } else if ($(this).hasClass("eraser")) {
                paintManager.changeColor("FFFFFF");
                toolType = "eraser";
            } else if ($(this).hasClass("undo")) {
                actionsLogger.Undo();
                $("#editor .toolbar .button.pencil").addClass("clicked");
                setTimeout(function() {
                    $("#editor .toolbar .button.undo").removeClass("clicked");
                }, 300);
            } else if ($(this).hasClass("redo")) {
                actionsLogger.Redo();
                $("#editor .toolbar .button.pencil").addClass("clicked");
                setTimeout(function() {
                    $("#editor .toolbar .button.redo").removeClass("clicked");
                }, 300);
            } else if ($(this).hasClass("new")) {
                var r = confirm("Are you sure you want to start a new canvas and discard your changes?");
                if (r == true) {
                    window.location.href = "www.w3schools.com/jsref/met_loc_replace.asp";
                }
            } else if ($(this).hasClass("save")) {
                paintManager.imgSave();
            } else if ($(this).hasClass("load")) {
                paintManager.load("image/png");
            } else if ($(this).hasClass("Text")) {
                paintManager.Text();
            }

            paintManager.tool_change(toolType);
        }
    });

    $("html").mousedown(function() {
        if (UiManager.timeoutObject) {
            window.clearTimeout(UiManager.timeoutObject);
        }

        UiManager.timeoutObject = setTimeout(function() {
            $("#consoleWrapper").hide();
            $("#textDescription").show();
        }, 25000);
    });

});